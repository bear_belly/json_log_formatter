#!/usr/bin/env python3
"""
This file is part of json_log_formatter.

json_log_formatter is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

json_log_formatter is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with json_log_formatter.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import re
from setuptools import setup, find_packages

HERE = os.path.abspath(os.path.dirname(__file__))
REQ = os.path.abspath(os.path.join(HERE, 'requirements.txt'))
README = os.path.abspath(os.path.join(HERE, 'README.rst'))


def read_requirements(path):
    if not os.path.exists(path):
        return []
    with open(path) as requirements:
        return [x.strip() for x in requirements.read().split('\n')
                if re.match(r'^[\d\w]', x)]

def get_readme(path):
    if not os.path.exists(path):
        return []
    with open(path) as readme:
        return readme.read()


setup(
    name="python3_json_log_formatter",
    version="1.6.1",
    packages=find_packages(),
    install_requires=read_requirements(REQ),
    # metadata for upload to PyPI
    author="Jordan Hewitt",
    author_email="jordan.h@startmail.com",
    description=get_readme(README),
    license="GPLv3",
    keywords="json log output format flannel"
)
